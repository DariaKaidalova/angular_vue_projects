import { Injectable } from '@angular/core';
import { LocationService } from './location.service';
import { StationModel } from '../models/station-model';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FilterService {

  constructor(private _locationService: LocationService) {}

  public findStation(id: string | number, stationsList: Array<StationModel>): StationModel {
  
    for(let i = 0; i < stationsList.length; i++) {
      if(stationsList[i].id == id) {
        return stationsList[i];
      }
    }
    return null;

  }

  public addDistance(stationList: Array<StationModel>): Observable<Array<StationModel>> {
     
    return this._locationService.getPosition()
      .pipe(
        map(pos => {
          if(pos !== null) {
            for(let i = 0; i < stationList.length; i++) {
              stationList[i].distance = this.calculateDistance(stationList[i], pos);
            }
          }

          return stationList;
      }));

  }

  public addDistanceToStation(station: StationModel): Observable<StationModel> {

    return this._locationService.getPosition()
      .pipe(map(pos => {
        if(pos !== null) {
          station.distance = this.calculateDistance(station, pos);
        }
        return station;
      }));
      
  }

  private calculateDistance(station: StationModel, position: any) {

    return this._locationService.getDistance(
      station.geometry.coordinates[1],
      station.geometry.coordinates[0],
      position.lat,
      position.lon);

  }

  public sortStations(stationsList: Array<StationModel> ): Array<StationModel> {

    stationsList.sort(function(currentStation, nextStation) {
      return currentStation.distance - nextStation.distance;
    });

    return stationsList;
  }

}

