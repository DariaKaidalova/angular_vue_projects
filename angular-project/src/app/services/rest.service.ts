import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, tap, flatMap } from 'rxjs/operators';
import { FilterService } from './filter.service'
import { StationModel } from '../models/station-model';

@Injectable({
  providedIn: 'root'
})
export class RestService {
  private _url = 'http://www.poznan.pl/mim/plan/map_service.html?mtype=pub_transport&co=stacje_rowerowe';
  private _stationsList: Array<StationModel> = [];

  constructor(
    private _httpClient: HttpClient,
    private _filterService: FilterService
    ) { }

  public get stationsList(): Observable<StationModel[]> {

    return of(this._stationsList);

  }

  public getStationsList() {

    this.getStationsListFromServer()
      .pipe(
        flatMap(data => this._filterService.addDistance(data.features)),
        tap(stations => this._filterService.sortStations(stations)),
        // tap(console.log)
      )
      .subscribe((data) => {
        this._stationsList.length = 0;
        this._stationsList.push(...data);
      });

  }

  private getStationsListFromServer(): Observable<any> {

    return this._httpClient.get(this._url)
      .pipe(
        // tap(console.log)
      );

  }

  public getStation(id: string | number): Observable<StationModel> {

    let stationObservable: Observable<StationModel>;

    if(this._stationsList.length === 0) {
      
      stationObservable = this.getStationsListFromServer()
        .pipe(
          // tap(console.log),
          map(data => this._filterService.findStation(id, data.features))
        )

    } else {

      stationObservable = of(this._filterService.findStation(id, this._stationsList));
    }

    return stationObservable.pipe(
      flatMap(station => this._filterService.addDistanceToStation(station))
    );

  }
  
}


