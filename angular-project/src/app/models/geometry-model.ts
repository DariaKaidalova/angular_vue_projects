import { GeometryInterface } from './geometry-interface';

export class GeometryModel {

  coordinates: [number, number];
  type: string;

  constructor(opts: Partial<GeometryInterface>) {
    opts: opts || {};
    this.coordinates = opts.coordinates;
    this.type = opts.type;
  }
  
}
