import * as uuid from 'uuid/v4';
import { StationInterface } from './station-interface';
import { GeometryModel } from './geometry-model';
import { PropertiesModel } from './properties-model';

export class StationModel {
  
  id: string;
  geometry: GeometryModel;
  properties: PropertiesModel;
  type: string;
  distance?: number;
  
  constructor(opts: Partial<StationInterface>) {

    opts: opts || {};
    this.id = opts.id || uuid();
    this.geometry = opts.geometry;
    this.properties = opts.properties;
    this.type = opts.type;
    this.distance = opts.distance;
  }
  
}
