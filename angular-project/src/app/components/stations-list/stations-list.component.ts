import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { RestService } from '../../services/rest.service'
import { StationModel } from 'src/app/models/station-model';

@Component({
  selector: 'app-stations-list',
  templateUrl: './stations-list.component.html',
  styleUrls: ['./stations-list.component.scss']
})
export class StationsListComponent implements OnInit {

  public stationsList$: Observable<StationModel[]>;
  public stationsList: Array<StationModel>;
  private sub: boolean = true;

  constructor(private _restService: RestService) { }

  ngOnInit() {

    this.stationsList$ = this._restService.stationsList;
    this._restService.getStationsList();

  }

  ngOnDestroy(): void {

    this.sub = false;

  }

}
