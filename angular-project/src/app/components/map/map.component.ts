import { Component, OnInit, Input } from '@angular/core';
import { GeometryModel } from 'src/app/models/geometry-model';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  @Input() geometry: GeometryModel;
  public url: string = '';

  constructor() { }

  ngOnInit() {

    this.url = `https://maps.google.com/maps?q=${this.geometry.coordinates[1]}%2C%20${this.geometry.coordinates[0]}&t=&z=17&ie=UTF8&iwloc=&output=embed`;
    
  }


}
